TARGET_OS := linux
TARGET_OS_FLAVOUR := generic
TARGET_LIBC := eglibc

TARGET_ARCH := riscv

TARGET_CROSS := $(ALCHEMY_WORKSPACE_DIR)/prebuilt/riscv64-linux-musl-cross/bin/riscv64-linux-musl-

HOST_CPP = cc -E

TARGET_IMAGE_FORMAT := cpio
TARGET_FINAL_MODE := firmware

TARGET_SKEL_DIRS := $(TARGET_VENDOR_DIR)/skel

TARGET_LINUX_DEVICE_TREE := starfive/jh7100-beaglev-starlight.dtb

